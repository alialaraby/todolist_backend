const db = require('../for-testing/db');
const mailer = require('../for-testing/mail');
const notifier = require('../for-testing/notifier');
const { fizzBuzz } = require('../middlewares/demo-testing-functions');
describe('FizzBuzz', () => {
    //1- throw if input isn`t number
    it('should throw', () => {
        expect(()=>{ fizzBuzz('asd') }).toThrow('can only accept number, but got string');
    });
    //2- return FizzBuzz in case input is divisable by 3 and 5
    it('return FizzBuzz', () => {
        expect(fizzBuzz(15)).toBe('FizzBuzz');
    });
    //3- return Fizz in case input is divisable by 3
    it('return FizzBuzz', () => {
        expect(fizzBuzz(9)).toBe('Fizz');
    });
    //4- return Buzz in case input is divisable by 5
    it('return FizzBuzz', () => {
        expect(fizzBuzz(5)).toBe('Buzz');
    });
    //5- return same input if not divisable by 3 or 5
    it('return same input', () => {
        expect(fizzBuzz(4)).toBe(4);
    });
});
describe('notifyUser', () => {
    it('should log data is sent with user email', () => {
        db.getUserById = jest.fn().mockReturnValue({ Id: 'someId', email: 'someEmail' });
        mailer.send = jest.fn();
        notifier.notifyUser(1);
        
        expect(db.getUserById).toHaveBeenCalled();
        expect(db.getUserById.mock.calls[0][0]).toBe(1);
        
        expect(mailer.send).toHaveBeenCalled();
        expect(mailer.send.mock.calls[0][0]).toBe('someEmail');
        expect(mailer.send.mock.calls[0][1]).toBe('items sent');
    })
})