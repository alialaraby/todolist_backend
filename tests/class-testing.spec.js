const SampleClass = require('../middlewares/class-sample');

describe('SampleClass', () => {
    const sampleC = new SampleClass();
    const getNameMock = jest.fn(() => 'ali');

    it('should contain a setName method', () => {
        expect(typeof sampleC.setName).toBe('function');
    })

    // it('should return nothing from setName method', () => {
    //     expect(sampleC.setName()).toBeUndefined();
    // })

    // it('setName to take a ali as name input and return nothing', () => {
    //     const setNameSpy = jest.spyOn(sampleC, 'setName');
    //     const result = sampleC.setName('ali');
    //     expect(result).toBeUndefined();
    //     expect(setNameSpy).toHaveBeenCalledWith('ali');
    //     setNameSpy.mockClear();
    // });

    // it('getName to return ali as a return', () => {
    //     const getNameSpy = jest.spyOn(sampleC, 'getName');
    //     expect(sampleC.getName()).toBe('ali');
    //     expect(getNameSpy).toHaveBeenCalledTimes(1);
    //     getNameSpy.mockClear();
    // })
    

})