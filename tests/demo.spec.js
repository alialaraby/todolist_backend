const isPositive = require('../middlewares/demo-testing-functions');
const isNumber = require('../middlewares/testing-demo-helpers');

jest.mock('../middlewares/testing-demo-helpers', () => jest.fn());

// afterEach(() => {
//     isNumber.mockClear();
// })
describe('isPositive', () => {
    const passingSamples = [0, 1, 2];
    const failingSamples = [-1, undefined, 'ali', null];

    //1- when isNumber return true with passing values for isPositive
    it.each(passingSamples)('should return true if sample is number and >= 0', (fixture) => {
        isNumber.mockImplementation(() => true);
        expect(isPositive.isPositive(fixture)).toBe(true);
        expect(isNumber).toHaveBeenCalledWith(fixture);
    });
    // //2- when isNumber return true with faling values for isPositive
    // it.each(failingSamples)('should return true if sample is number and >= 0', (fixture) => {
    //     isNumber.mockImplementation(() => true);
    //     expect(isPositive.isPositive(fixture)).toBe(false);
    // });
    // //3- when isNumber returns false with passing values for isPositive
    // it.each(passingSamples)('should return false otherwise', (fixture) => {
    //     isNumber.mockImplementation(() => false);
    //     expect(isPositive.isPositive(fixture)).toBe(false);
    // });
    // //4- when isNumber returns false with failing values for isPositive
    // it.each(failingSamples)('should return false otherwise', (fixture) => {
    //     isNumber.mockImplementation(() => false);
    //     expect(isPositive.isPositive(fixture)).toBe(false);
    // });

})

// describe('isNumber', () => {
//     const passingSamples = [-1, 0, 1, 2];
//     const failingSamples = [undefined, 'ali'];

//     it.each(passingSamples)('should return true if value is valid number', (fixture) => {
//         expect(isNumber(fixture)).toBe(true);
//     });
//     it.each(failingSamples)('should return false if value is invalid number', (fixture) => {
//         expect(isNumber(fixture)).toBe(false);
//     });
// })