const express = require('express');
const app = express();
const userRouter = require('../routers/user');
app.use('/users', userRouter);
const userModel = require('../models/user.js');
const mongoose = require('mongoose');
const config = require('config');
const supertest = require('supertest');
const userService = require('../services/user');
const { generateToken } = require('../middlewares/auth');
const jwt = require('jsonwebtoken');
// beforeEach(() => {
//     mongoose.connect(
//         config.get('db_connectionString'), 
//         { useNewUrlParser: true, useUnifiedTopology: true }
//     );
//     userModel.create({fullName: 'ali', username: 'alialaraby', password: '123aaAA23**', email: 'alisakr@grmd.com'});
// });
// afterAll(() => {
//     mongoose.connection.close();
// });
// describe('GET users/getAllUsers', () => {
//     it('should return status code 200/204 object with array of users and message', async () => {
//         const user = await userModel.create({fullName: 'ali', username: 'alialaraby', password: '123aaAA23**', email: 'alisakr@grmd.com'});
//         await supertest(app).get('/users/getAllUsers')
//         .expect(200)
//         .then((response) => {
//             expect(response.body).toHaveProperty('data');
//             expect(response.body.data.length).toBeTruthy();
//             expect(response.body).toHaveProperty('message');
//         });
//     });
// });
// describe('userService login', () => {
//     it('should take username and password and return successful login and user data', () => {
//         let userCreds = { username: 'alialaraby', password: '123aaAA23**' };
//         expect(userCreds).toMatchObject(toMatchObject)
//         expect(userService.login(userCreds))
//     })
// })

test('Sanity Check', () => {
    expect(true).toBe(true); 
});

//test token generating method
test('generateToken to generate jwt token with user id payload', () => {
    let token = jwt.sign({ userId: 'someId' }, config.get('auth_key'));
    const generatedToken = generateToken('someId');
    expect(generatedToken).toBe(token);
})