module.exports = class {
    name = '';
    constructor(){}

    setName(name){
        this.name = name;
    }
    getName(){
        return this.name;
    }

    sayHello(){
        return `hi there my name is ${this.name}`;
    }
}