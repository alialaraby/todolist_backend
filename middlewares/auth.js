const jwt = require('jsonwebtoken');
const config = require('config');
const unsecurePaths = Object.values(require('../enums/unsecure-paths'));
const userService = require('../services/user');

exports.authUser = (req, res, next) => {
    try {
        if(unsecurePaths.includes(req.originalUrl)) next();
        else{
            const token = req.headers.authorization.split(' ')[1] || req.query.token || req.headers["x-access-token"];
            if(!token) throw 'unauthorized';
            const decodedToken = jwt.verify(token, config.get('auth_key'));
            if(!decodedToken.userId) throw 'unauthorized';
            if(decodedToken.userId != req.body.userId) throw 'unauthorized';
            next();
        }
    } catch (error) {
        res.status(401).json({ message: 'authorization error, check your token or userId' });
    }
}

exports.authorizeUser = (roles = []) => {
    return async (req, res, next) => {
        let user = null;
        try {
            user = await userService.getUserById(req.body.userId);
            if(roles.includes(user.role)) next();
            else{
                res.status(401).json({ message: 'authorization error, unauthorized to get such resource' });
            }
        } catch (error) { next(error) }
    }
}

exports.generateToken = (userId) => {
    return jwt.sign({ userId: userId }, config.get('auth_key'));
}

// exports.generateToken = (userId, expiresIn = 0) => {
//     const token = (expiresIn === 0) ? 
//         jwt.sign({ userId: userId }, config.get('auth_key')) :
//         jwt.sign({ userId: userId }, config.get('auth_key'), { expiresIn: `${expiresIn}h` });

//     return token;
// }