const isNumber = require('../middlewares/testing-demo-helpers');

exports.isPositive = (numberToTest) => ((!numberToTest && numberToTest !== 0) || !isNumber(numberToTest)) ? false : 
    (numberToTest >= 0) ? true : false;

exports.fizzBuzz = (input) => {
    if(typeof input !== 'number') throw new Error(`can only accept number, but got ${typeof input}`);

    if((input % 3 === 0) && (input % 5 === 0)) return 'FizzBuzz';

    if(input % 3 === 0) return 'Fizz';

    if(input % 5 === 0) return 'Buzz';

    return input;
}