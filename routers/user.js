// module.exports = function (app) {
//     const userController = require('../controllers/user');

//     app.post('/insrtUser', userController.insertUser)
// }

//here`s the second way to use routers just to state it (the above is much easier)

const userController = require('../controllers/user');
const express = require('express');
const userRouter = express.Router();
const error = require('../middlewares/error');
const auth = require('../middlewares/auth');

userRouter.post('/registerUser', userController.registerUser);
userRouter.post('/login', userController.login);
userRouter.post('/getUserById', auth.authorizeUser(['Admin', 'User']), userController.getUserById);
userRouter.post('/getAllUsers', auth.authorizeUser(['Admin']), userController.getAllUsers);
userRouter.use(error);
module.exports = userRouter;