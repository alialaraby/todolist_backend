//imports
const express = require('express');
const app = express();
const debug = require('debug')('app:all'); // condotional log, choose to show these log or not from env
require('dotenv').config(); // to read vars from .env file

const redis = require('redis');
const redisClient = redis.createClient(process.env.REDIS_PORT);
const axios = require('axios');

process.on('unhandledRejection', (ex) => { throw ex });

require('./startup/cors')(app);
require('./startup/config')(app, debug);
require('./startup/db')(debug)
require('./startup/routes')(app, debug)
require('./startup/port')(app, debug);

// const problemSolving = require('./middlewares/prob-solving');
function isValidWalk(walk = []) {
    if(walk.length !== 10 ) return false;
    let nCount = 0, sCount = 0, eCount = 0, wCount = 0;
    nCount = walk.filter(x => x === 'n').length;
    sCount = walk.filter(x => x === 's').length;
    eCount = walk.filter(x => x === 'e').length;
    wCount = walk.filter(x => x === 'w').length;

    return (nCount === sCount && eCount === wCount) ? true : false;
}
console.log(isValidWalk(['n','s','n','s','n','s','n','s','n','s']));

// async function getRepos(req, res, next) {
//     try {
//         console.log('fetching data', req.params.username);
        
//         const response = await axios.get(`https://api.github.com/users/alialaraby`);
//         const repos = response.data.public_repos;

//         // set data in redis
//         redisClient.setex(req.params.username, 3600, repos);

//         res.status(200).json({ data: `${req.params.username} has ${repos} github repos` });
//     } catch (error) {
//         console.log(error);
//         res.status(500);
//     }
// }

// function cache(req, res, next) {
//     let username = req.params.username;

//     redisClient.get(username, (error, data) => {
//         if(error) throw error;

//         if(data != null) res.status(200).json({ data: `${req.params.username} has ${data} github repos` });
//         else next();
//     })
// };

// app.get('/repos/:username', cache, getRepos);

module.exports = { app: app, debug: debug };